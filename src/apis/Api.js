import axios from 'axios';

const baseUrl = 'https://www.omdbapi.com?apiKey=ffd0c3a5';

export const apiCall = (method, url, data, headers) => axios({
    method,
    url: baseUrl + url,
    data,
    headers
});