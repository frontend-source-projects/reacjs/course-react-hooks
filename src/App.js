import React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';

import './App.css';

import Home from './pages/home/Home';
import Results from './pages/results/Results';

const App = () => (
    <Router>
      <div>
        <Route exact path = "/" component = {Home}></Route>
        <Route path = "/results" component = {Results}></Route>
      </div>
    </Router>
);

export default App;