import React, {useState, useEffect} from 'react';
import { Container, Grid } from '@material-ui/core';
import queryString from 'query-string';

import { apiCall } from '../../apis/Api';
import ResultStyles from './ResultStyles';

function Results({location}) {
    const classes = ResultStyles();

    const {search} = location;

    const {movieName} = queryString.parse(search);

    const [responseAPI, setResponseAPI] = useState({ status: 404, data: {} });

    useEffect(() => {
        const searchApiCall = async () => {
          const consulta = await apiCall("GET", "&t=" + movieName);
     
          console.log(queryString.parse(search));
          console.log(movieName);
          console.log(consulta);
          setResponseAPI(consulta);
        };
     
        searchApiCall();
    },[movieName, search]);

    return (
        <Container className = {classes.container}>
            <Grid className = {classes.titleGridContainer}>
                <Grid>Result</Grid>
            </Grid>
            <Grid className = {classes.generalGridContainer}>
                <Grid className = {classes.individualGridContainer}>Name</Grid>
                <Grid className = {classes.individualGridContainer}>Year</Grid>
            </Grid>
            <Grid className = {classes.generalGridContainer}>
                <Grid className = {classes.individualGridContainer}>{responseAPI.data.Title}</Grid>
                <Grid className = {classes.individualGridContainer}>{responseAPI.data.Year}</Grid>
            </Grid>
        </Container>
    );
}

export default Results;