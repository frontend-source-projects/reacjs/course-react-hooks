import {makeStyles} from '@material-ui/styles';

export const centerCommonStyle = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
}

export default makeStyles({

    container: {
        ...centerCommonStyle,
        height: '100vh',
        flexDirection: 'column'
    },
    cardContainer: {
        ...centerCommonStyle,
        flexDirection: 'column',
        width: 400,
        height: 200,
        padding: '2rem'
    },
    titleGridContainer: {
        ...centerCommonStyle,
    },
    title: {
        fontSize: '4rem'
    },
    textFieldSearch: {
        width: '90%'
    },
    searchButton: {
        marginLeft: '.5rem'
    },
    buttonsContainer: {
        marginTop: '.5rem'
    },
    movieIcon: {
        fontSize: '4rem'
    }

});