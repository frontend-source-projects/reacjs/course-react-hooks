import React, {useState} from 'react';
import {Container, Typography, Card, Grid, TextField, Button} from '@material-ui/core'
import {MovieIcon} from '../../icons/Icon';
import { useHistory } from 'react-router-dom';

import HomeStyles from './HomeStyles'

function Home () {
  
  const history = useHistory();

  const [searchText, setSearchText] = useState('');  
  
  const handleSearchTextChange = event => {
    setSearchText(event.target.value);
  }

  const cleanSearch = event => {
    setSearchText('');
  }

  const handlerSearch = event => {
    history.push("/results?movieName=" + searchText);
  }

  const classes = HomeStyles();

  return (
    <Container className = {classes.container}>
      <Card className = {classes.cardContainer}>
        <Grid container className = {classes.titleGridContainer}>
          <Grid><Typography className = {classes.title}>Bienvenido</Typography></Grid>
          <Grid><MovieIcon className = {classes.movieIcon}></MovieIcon></Grid>
        </Grid>
        <TextField value = {searchText} placeholder = "Buscar..." onChange = {handleSearchTextChange} className = {classes.textFieldSearch}/>
        <Grid className = {classes.buttonsContainer}>
          <Button variant = "contained" onClick = {cleanSearch}>Limpiar</Button>
          <Button variant = "contained" onClick = {handlerSearch} className = {classes.searchButton}>Buscar</Button>
        </Grid>
      </Card>
    </Container>
  );
}

export default Home;